pragma solidity ^0.5.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";



contract Sms is ERC20 {
    address private tokengiver=0xdD870fA1b7C4700F2BD7f44238821C26f7392148;
  address private receiver=0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160C;
  uint maxallowed=200;
  uint tokkensgenerated;
  uint256 expirationdate;
  
  constructor (uint tokkens) public {
      _mint(tokengiver,tokkens);
      tokkensgenerated=tokkens;
  }
  
  function transfering() public {
require(maxallowed>tokkensgenerated);
_transfer(tokengiver, receiver, tokkensgenerated);
expirationdate= now + 3 ;

  }
  function removetokens() public {
      require (now > expirationdate);
      _burn(receiver,tokkensgenerated);
  }
  
  
}